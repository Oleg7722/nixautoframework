package nix.traning.autointensive.page

import geb.Page

class StartPage extends Page {
    static content = {
        productsLink(wait: true) { $("a", text: "Products") }
        blogLink(wait: true) { $("a", text: "Blog") }
    }

    static at = {
        title == "NIX – Outsourcing Offshore Software Development Company"
    }

    def "User navigates to Product page"() {
        productsLink.click()
    }

    def "User navigates to Blog page"() {
        blogLink.click()
    }
}
