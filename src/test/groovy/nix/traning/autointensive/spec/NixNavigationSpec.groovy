package nix.traning.autointensive.spec

import geb.spock.GebReportingSpec
import nix.traning.autointensive.page.StartPage
import nix.traning.autointensive.page.BlogPage

class NixNavigationSpec extends GebReportingSpec {
    def "Navigate to Blog page"(){
        when:
        to StartPage

        and:
        "User navigates to Blog page"()

        then:
        at BlogPage
    }
}
